include("blpapi.jl")
include("database.jl")
include("svi.jl")
using Dates
import Serialization

baseDate = Date(2020, 1, 20)
ticker = "KOSPI2 Index"

# get spot
ret = hist(ticker, "PX_LAST", baseDate, baseDate)
spot = ret[1, 2]
@info ret

# get Option price from database
ticker = "KOSPI2"
sql = """select a.nalja,
                a.ticker,
                a.bid,
                a.ask,
                a.last_price,
                b.opt_strike_px,
                b.type,
                b.last_tradeable_dt
  from fent_blp_opt_quote a,
       fent_blp_opt_info b
 where a.nalja='$(baseDate)'
   and a.ticker like '$(ticker)%'
   and a.ticker=b.ticker
   and b.type in ('C', 'P')"""
println(sql)
ret = select_sql(sql)
@info ret

Serialization.serialize("ret.jld", ret)
options = ref(ticker, "OPT_CHAIN")
println(options[1])
println(typeof(options[1]))
@info options[1]["fieldData"]["OPT_CHAIN"][1]
ret[!, "mid"] = (ret[!, "ASK"] + ret[!, "BID"])/2
ret["ASK"]
ret[:, "ASK"] + ret[:, "BID"]
ret[:, :ASK]
first(ret, 6)


ret = ref(ticker, "OPT_CHAIN")
println(ret)

# startDate = Date(2020, 12, 1)
# endDate = Date(2020, 12, 30)

# ret = hist(ticker, "PX_LAST", startDate, endDate)
# @info first(ret, 6)
