using PyCall
using Conda
using DataFrames

ENV["PATH"] = "D:\\fng\\instantclient\\database\\instantclient_12_2_x64;D:\\fng\\instantclient\\database\\instantclient_12_2_x64\\bin;" * ENV["PATH"]

function select_sql(sql::String)
    cx_Oracle = pyimport("cx_Oracle")
    connect = cx_Oracle.connect("ELSDBA", "dwsea#01", "DS76E_DIRECT")
    cursor = connect.cursor()
    cursor.execute(sql)
    ret = cursor.fetchall()
    cursor.description
    dict = Dict()
    for (i, k) in enumerate(cursor.description)
        dict[Symbol(k[1])] = [x[i] for x in ret]
    end
    df = DataFrame(dict)
    cursor.close()
    connect.close()
    return df
end

function option_price(baseDate::Date, ticker::String)
    sql = """select a.nalja,
                    a.ticker,
                    a.bid,
                    a.ask,
                    a.last_price,
                    b.opt_strike_px as strike,
                    b.type,
                    b.last_tradeable_dt as expiry
      from fent_blp_opt_quote a,
           fent_blp_opt_info b
     where a.nalja='$(baseDate)'
       and a.ticker like '$(ticker)%'
       and a.ticker=b.ticker
       and b.type in ('C', 'P')"""
    ret = select_sql(sql)
    ret[:, ["NALJA", "TICKER", "TYPE", "STRIKE", "EXPIRY", "BID", "ASK", "LAST_PRICE"]]
end