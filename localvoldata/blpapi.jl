using HTTP
using JSON
using DataFrames
using Dates


function ref(ticker::String, field::String)
	res = HTTP.request("GET", "http://172.21.35.174:31235/diva/api/ref?securities=$(ticker)&fields=$(field)")
	res.status
	ress = String(res.body)
	JSON.parse(ress)
end

function hist(ticker::String, field::String, startDate::Date, endDate::Date)
	convert_str(x) = Dates.format(x, "yyyymmdd")
	url = "http://172.21.35.174:31235/diva/api/hist?securities=$(ticker)&fields=$(field)&startDate=$(convert_str(startDate))&endDate=$(convert_str(endDate))"
	res = HTTP.request("GET", url)
	res.status
	ress = String(res.body)
	ret = JSON.parse(ress)
	dictlist = ret[1]["fieldData"]
	df = DataFrame([NamedTuple{Tuple([Symbol(k) for k in keys(d)])}(values(d)) for d in dictlist])
end
