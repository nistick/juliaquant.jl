
struct RawSVI
	a::Float64
	b::Float64
	ρ::Float64
	m::Float64
	σ::Float64
	t::Float64
end

function totalVariance(svi::RawSVI, k::Float64)
	svi.a + svi.b *(svi.ρ*(k-svi.m) + sqrt((k-svi.m)^2 + svi.σ^2))
end
	

struct JumpWingSVI
	v::Float64
	ψ::Float64
	p::Float64
	c::Float64
	v̂::Float64
end

function rawSVItoJWSVI(svi::RawSVI, t::Float64)
	v = totalVariance(svi, 0.0)
	w_t = v * t
	ψ = 1/sqrt(wt) * svi.b/2 * (-svi.m/sqrt(svi.m^2 + svi.σ^2) + svi.ρ)
	p = 1/sqrt(wt) * svi.b *(1-svi.ρ)
	c = 1/sqrt(wt) * svi.b *(1-svi.ρ)
	v̂ = 1/t*(svi.a + svi.b * svi.σ*sqrt(1-svi.ρ^2))
	JumpWingSVI(v, ψ, p, c, v̂)
end
