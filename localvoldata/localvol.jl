S = 3485.84
S_ = S * 0.99
@info S_
@info 1950 / S

@info 1950 * 0.99
@info 1950 * 0.99 / S_

@info 1950 / S_ #
@info log(0.1)

@info 1950 / 3445.9133069020645

dt = 0.0054794520547945258
dk = 0.012875503299472957
npv_dt= 2.9802875870696423e-14
npv_up = 1.3349879029018860e-13
npv_dn = 0.0
npv = 6.5752326514007928e-14

theta = (npv-npv_dt)/dt
gamma = (npv_up -2*npv + npv_dn)/dk^2
delta= (npv_up-npv_dn)/(2*dk)
var = 2 * theta / ( gamma - delta)
sqrt(var)
@info "values" theta gamma delta var sqrt(var)

1.0194060323367883e-16

		# forwards[0] =	3445.9133069020645	double
		# forwards[1]	 = 3439.3564069954755	double
		# forwards[2]	 - 3436.0008339857804	double
		# forwards[3]	3387.3601659793594	double
		# forwards[4]	3372.0998753192234	double
		# forwards[5]	3359.2440184864486	double
		# forwards[6]	3287.3523060513803	double
		# forwards[7]	3262.1179559411435	double
		# forwards[8]	3190.8555644624689	double
		# forwards[9]	3170.3764302430595	double
		# forwards[10]	3087.2227585534861	double
