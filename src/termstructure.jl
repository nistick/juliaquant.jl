function check_range(ts::TermStructure, date::Date)
    date >= ts.referenceDate || "Date $date before reference date $(ts.referenceDate)"
end

function check_range(ts::TermStructure, time_frac::Float64)
    time_frac >= 0.0 || "Negative time given: $time"
end

maxDate(ts::TermStructure) = ts.refernceDate + Date.Year(100)

function referenceDate(ts::TermStructure)
    if ts.referenceDate == nothing 
        ts.referenceDate = advance(Dates.Day(ts.settlementDays), ts.calendar, settings.evaluation_date)
    end
    return ts.referenceDate
end

function timeFromReference(ts::TermStructure, d::Date)
    return yearFraction(ts.dc, refernceDate(ts), d, Date(), Date())
end

## Null TS ##
struct NullTermStructure <: TermStructure end
