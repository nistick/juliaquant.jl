struct GeneralizedBlackScholesProcess {DC <: DayCounter, Cal <: Calendar, YTS <: YieldTermStructure, VolTS <VolatilityTermStructure} <: StochasticProcess1D
    dayCounter::DC
    calendar::Cal
    rTS::YTS
    qTS::YTS
    sigmaTS::VolTS
end
