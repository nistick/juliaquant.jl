# observer
abstract type Observer end

# Lazy Object
abstract type LazyObject <: Observer end

abstract type DayCounter end
abstract type Calendar end

# Instruments
abstract type Instrument end
abstract type Payoff end
abstract type TypePayoff <: Payoff end
abstract type StrikedTypePayoff <: TypePayoff end
abstract type OptionType end
abstract type Option{E} <: Instrument end
abstract type OneAssetOption{E} <: Option{E} end

# Methods
abstract type FdScheme end
abstract type FdmMesher end
abstract type Fdm1DMesher end
abstract type StepCondition end
abstract type CurveWrapper end
abstract type StepType end
abstract type FdmInnerValueCalculator end
abstract type FdmLinearOpComposite end

# PricingEngine
abstract type PricingEngine end

# StochasticProcess
abstract type StochasticProcess end
abstract type StochasticProcess1D <: StochasticProcess end

# Exercise
abstract type Exercise end
abstract type ExerciseType end

# TermStructure
abstract type TermStructure end
abstract type YieldTermStructure <: TermStructure end
abstract type FilterImpliedVolatility end

# Time
abstract type Calendar end

# InterestRate
abstract type Compounding end
