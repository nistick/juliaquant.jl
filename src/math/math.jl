module Math
export LinearInterpolation, Interpolation, value,
# optimization/optimization.jl
OptimizationMethod, CostFunction, Constrait

include("interpolation.jl")
include("interpolations/linearinterpolation.jl")
include("optimization/optimization.jl")
end
