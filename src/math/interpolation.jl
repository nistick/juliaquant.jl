abstract type Interpolation end

function update!(interp::Interpolation, idx::Int, val::Float64)
    interp.y_vals[idx] = val
    update!(interp, idx)
    return interp
end

function locate(interp::Interpolation, val::Float64)
    if val < interp.x_vals[1]
        return 1
    elseif val > interp.x_vals[end-1]
        return length(interp.x_vals) - 1
    else
        return searchsortedlast(interp.x_vals, val)
    end
end
