mutable struct LinearInterpolation <: Interpolation
    x_vals::Vector{Float64}
    y_vals::Vector{Float64}
    s::Vector{Float64}
end

function LinearInterpolation(x_vals::Vector{Float64}, y_vals::Vector{Float64})
    s = (y_vals[2:end] - y_vals[1:end-1])./(x_vals[2:end] - x_vals[1:end-1])
    return LinearInterpolation(x_vals, y_vals, s)
end

function LinearInterpolation()
    x_vals = Vector{Float64}()
    y_vals = Vector{Float64}()
    s = Vector{Float64}()
    return LinearInterpolation(x_vals, y_vals, s)
end

function initialize!(interp::LinearInterpolation, x_vals::Vector{Float64}, y_vals::Vector{Float64})
    interp.x_vals = x_vals
    interp.y_vals = y_vals
    interp.s = zeros(length(y_vals))
    return interp
end

function update!(interp::LinearInterpolation, idx::Int)
    @simd for i = 2:idx
        @inbounds dx = interp.x_vals[i] - interp.x_vals[i-1]
        @inbounds interp.s[i-1] = (interp.y_vals[i] - interp.y_vals[i-1])/dx
    end
    return interp
end

update!(interp::LinearInterpolation) = update!(interp, length(interp.y_vals))

function value(interp::LinearInterpolation, val::Float64)
    i = locate(interp, val)
    return interp.y_vals[i] + (val - interp.x_vals[i]) * interp.s[i]
end

function derivate(interp::LinearInterpolation, val::Float64)
    i = locate(interp, val)
    return interp.x[i]
end
