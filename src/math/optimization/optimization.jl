abstract type OptimizationMethod end
abstract type CostFunction end
abstract type Constrait end

mutable struct EndCriteria
    maxIterations::Int
    maxStationaryStateIterations::Int
    rootEpsilon::Float64
    funtionEplsilon::Float64
    gradientNormEpsilon::Float64
end
