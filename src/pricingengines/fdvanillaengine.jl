mutable struct FDEuropeanEngine{B <: AbstractBlackScholesProcess} <: AbstractFDVanillaEngine
    process::B
    tGrid::Int
    xGrid::Int
    dampingSteps::Int
    localVol::Bool
    illegalLocalVolOverwrite::Real
end

function FDEuropeanEngine(process::B,
                          tGrid::Int = 100,
                          xGrid::Int = 100,
                          dampingSteps::Int = 0,
                          localvol::Bool = false,
                          illegalLocalVolOverwrite::Float64 = 0.25
                          ) where B <: GeneralizedBlackScholesProcess
    return FDEuropeanEngine(process, tGrid, xGrid, dampingSteps, localvol, illegalLocalVolOverwrite)
end
