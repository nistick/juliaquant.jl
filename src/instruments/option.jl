mutable struct OptionResults
    delta::Float64
    gamma::Float64
    theta::Float64
    vega::Float64
    rho::Float64
    dividendRho::Float64
    value::Float64
end

OptionResults() = OptionResults(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

struct VanillaOptionArgs{P <: StrikedTypePayoff, E <: Exercise}
    payoff::P
    exercise::E
end

function reset!(res::OptionResults)
    res.delta = 0.0
    res.gamma = 0.0
    res.theta = 0.0
    res.vega = 0.0
    res.rho = 0.0
    res.dividendRho = 0.0
    res.value = 0.0
    return res
end


mutable struct VanillaOption{S <: StrikedTypePayoff, E <: Exercise, P <: PricingEngine} <: OneAssetOption{E}
    lazyMixin::LazyMixin
    payoff::S
    exercise::E
    pricingEngine::P
    results::OptionResults
end

function VanillaOption(payoff::S, exercise::E) where {S <: StrikedTypePayoff, E <: Exercise, P <: PricingEngine}
    return VanillaOption{S, E, P}(LazyMixin(), payoff, exercise, nothing, OptionResults())
end

VanillaOption(payoff::S, exercise::E, pe::P) where {S <: StrikedTypePayoff, E <: Exercise, P <: PricingEngine} =
              VanillaOption{S, E, P}(LazyMixin(), payoff, exercise, pe, OptionResults())
