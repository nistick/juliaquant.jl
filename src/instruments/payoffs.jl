struct Put <: OptionType end
struct Call <: OptionType end

function optionType(payoff::TP) where {TP <: TypePayoff}
    return payoff.optionType
end

function description(payoff::TP) where { TP <: TypePayoff }
    return name(payoff) * " " * optionType(payoff)
end


struct PlainVanillaPayoff <: StrikedTypePayoff
    optionType::OptionType
    strike::Float64
end

function name(payoff::PlainVanillaPayoff)
    return "Vanilla"
end



(payoff::PlainVanillaPayoff)(price::Float64) = _get_payoff(payoff.optionType, payoff, price)

_get_payoff(::Call, payoff::PlainVanillaPayoff, price::Float64) = max(price - payoff.strike, 0.0)
_get_payoff(::Put, payoff::PlainVanillaPayoff, price::Float64) = max(payoff.strike - price, 0.0)
