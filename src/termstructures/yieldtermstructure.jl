function discount(yts::YTS, t::Float64) where YTS <: YieldTermStructure
    return discountImpl(yts, t)
end

function discount(yts::YTS, date::Date) where YTS <: YieldTermStructure
    yf = yearFraction(yts.dayCounter, referenceDate(yts), date)
    return discount(yts, yf)
end
