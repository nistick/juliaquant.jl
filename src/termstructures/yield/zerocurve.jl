abstract type ZeroYieldStructure <: YieldTermStructure end

function discountImpl(zys::TS, t::Float64) where TS <: ZeroYieldStructure
    if t == 0
        return 1.0
    end
    r = zeroYieldImpl(zys, t)
    return exp(-r * t)
end

struct InterpolatedZeroCurve{P <: Interpolation, DC <: DayCounter, B <: Calendar } <: ZeroYieldStructure
    settlementDays::Int
    referenceDate::Date
    dayCounter::DC
    calendar::B
    interp::P
    dates::Vector{Date}
    times::Vector{Float64}
    data::Vector{Float64}
end

function InterpolatedZeroCurve(settlementDays::Int,
                               referenceDate::Date,
                               dc::DC,
                               cal::B,
                               interpolator::P,
                               dates::Vector{Date},
                               data::Vector{Float64}) where {DC <: DayCounter, B <: Calendar, P <: Interpolation}
    idc = InterpolatedZeroCurve(settlementDays, referenceDate, dc, NullCalendar(), interpolator,
    dates, zeros(length(dates)), data)
    initialize!(idc)
    return idc
end

function initialize!(idc::InterpolatedZeroCurve)
    length(idc.dates) == length(idc.data) || error("dates / data mismatch")
    idc.times[1] = 0.0
    @simd for i = 2:length(idc.dates)
        @inbounds idc.times[i] = yearFraction(idc.dayCounter, idc.dates[1], idc.dates[i])
    end

    # initialize interpolator
    Math.initialize!(idc.interp, idc.times, idc.data)
    Math.update!(idc.interp)
    return idc
end

function zeroYieldImpl(idc::InterpolatedZeroCurve, t)
    if t <= idc.times[end]
        return value(idc.interp, t)
    end
    tMax = idc.times[end]
    zMax = idc.data[end]
    instFwdMax = zMax + tMax * derivative(idc.interpoation, tMax)
    return (zMax * tMax + instFwdMax * (t-tMax))/t
end
