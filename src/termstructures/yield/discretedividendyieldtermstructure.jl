mutable struct DiscreteDividendYieldTermStructure{DC <: DayCounter, Cal <: Calendar} <: YieldTermStructure
    referenceDate::Date
    settlementDays::Int
    dayCounter::DC
    calendar::Cal
    dates::Vector{Date}
    times::Vector{Float64}
    quotes::Vector{Float64}
end

function DiscreteDividendYieldTermStructure(referenceDate::Date,
    settlementDays::Int,
    dayCounter::DC,
    calendar::Cal,
    dates::Vector{Date},
    quotes::Vector{Float64}) where {Cal <: Calendar, DC <: DayCounter}
    times = [yearFraction(dayCounter, referenceDate, x) for x in dates]
    return DiscreteDividendYieldTermStructure(referenceDate, settlementDays, dayCounter, calendar, dates, times, quotes)
end


function discountImpl(yts::DiscreteDividendYieldTermStructure, yf::Float64)
    ret = 1.0
    for v in zip(yts.times, yts.quotes)
        if v[1] < yf
            ret *= (1-v[2])
        end
    end
    return ret
end
