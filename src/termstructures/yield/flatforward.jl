struct FlatForward{Cal <: Calendar, DC <: DayCounter, Comp <: Compounding} <: YieldTermStructure
    referenceDate::Date
    settlementDays::Int
    forward::Quote
    calendar::Cal
    dayCounter::DC
    comp::Comp
    freq::Frequency
end


function FlatForward(referenceDate::Date,
                    forward::Quote, dc::DC,
                    comp::Comp = Continuous(),) where {Cal <: Calendar, DC <: DayCounter, Comp <: Compounding}
    return FlatForward(referenceDate, 0, forward, NullCalendar(), dc, Continuous(), Annual)
end

function discountImpl(FF::FlatForward, t::Float64)
    return exp(-FF.forward.value * t)
end
