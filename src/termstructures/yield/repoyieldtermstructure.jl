struct RepoYieldTermStructure{DC <: DayCounter, Cal <: Calendar} <: YieldTermStructure
    referenceDate::Date
    settlementDays::Int
    dayCounter::DC
    calendar::Cal
    dates::Vector{Date}
    times::Vector{Float64}
    quotes::Vector{Float64}
end


function RepoYieldTermStructure(referenceDate::Date,
    settlementDays::Int,
    dayCounter::DC,
    calendar::Cal,
    dates::Vector{Date},
    quotes::Vector{Float64}) where {Cal <: Calendar, DC <: DayCounter}
    times = [yearFraction(dayCounter, referenceDate, x) for x in dates]
    return RepoYieldTermStructure(referenceDate, settlementDays, dayCounter, calendar, dates, times, quotes)
end


function discountImpl(yts::RepoYieldTermStructure, t::Float64)
    ret = 1.0
    previousTime = 0.0
    if t <=previousTime; return ret end
    for data in zip(yts.times, yts.quotes)
        if t < data[1]
            ret *= exp(data[2]*(t-previousTime))
            break
        else
            ret *= exp(data[2]*(data[1]-previousTime))
        end
        previousTime = data[1]
    end
    if yts.times[end] < t
        ret *= exp(yts.quotes[end]*(t-yts.times[end]))
    end
    return ret
end
