struct CalibrationSet
  option::VanillaOption
  sigma::Float64
end

struct PreminumFilter <: FilterImpliedVolatility
  callBound::Float64
  putBound::Float64
end

is_valid(filter::PreminumFilter) = true


@enum InterpolationType Linear
@enum CalibrationType CallSlice PutSlice CallPut
struct AndreasenHugeCostFunction
  previousNPVs::Vector{Float64}
  lnMarketStrikes::Vector{Float64}
  interpolationType::InterpolationType
  mesher::FdmMesherComposite
end

function solveFor(cost::AndreasenHugeCostFunction, sig::Vector{Float64}, previousNPVs::Vector{Float64})
    x = similar(cost.lnMarketStrikes)
    if cose.interpolationType == Linear
        sigInerpl = Spline1D(lnMarketStrikes, sig, k=1)
    else
      error("unknown interpolationi method")
    end
end


function value(costFunction::AndreasenHugeCostFunction, x::Vector{Float64})

end

struct AndreasenHugeVolatilityInterpl{YTS <: YieldTermStructure, OP <: OptimizationMethod, F <: FilterImpliedVolatility}
  calibrationSets::Vector{CalibrationSet}
  rTS::YTS
  qTS::YTS
  pTS::YTS
  filter::F
  interpolationType::InterpolationType
  nGridPoints::Int
  minStrike::Float64
  maxStrike::Float64
  optimizationMethod::OP
  endCriteria
end
