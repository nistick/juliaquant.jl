using Logging
struct Actual365 <: DayCounter end

function daysBetween(d1::Date, d2::Date)
    return Dates.value(d2 - d1)
end



function yearFraction(dc::Actual365, d1::Date, d2::Date)::Float64
    ret = daysBetween(d1, d2)
    return ret / 365.0
end
