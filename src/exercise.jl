struct American <: ExerciseType end
struct Bermudan <: ExerciseType end
struct European <: ExerciseType end

struct EuropeanExercise <: Exercise
    dates_::Vector{Date}
end

function EuropeanExercise(date::Date)
    dates = [date]
    return EuropeanExercise(dates)
end
