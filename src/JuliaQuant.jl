module JuliaQuant

include("math/math.jl")
using Dates
export Date

using JuliaQuant.Math


export CalibrationSet,
        AndreasenHugeVolInterpl,
        YieldTermStructure,
        DayCounter,
        Actual365,
        Compounding, FdmLinearOpLayout,

# abstracttypes.jl
Calendar, Compounding, PricingEngine, OneAssetOption, FilterImpliedVolatility,
# instruments/payoffs.jl
OptionType, Call, Put,
        PlainVanillaPayoff, StrikedTypePayoff,
# instruments/option.jl
VanillaOption,
# methods/finitedifferences/mesher.jl
Uniform1DMesher,

# exercise.jl
Exercise, EuropeanExercise,

# daycounter.jl
Actual365,
# processes/blackscholesprocess.jl
GeneralizedBlackScholesProcess,
# quotes/quotes.jl
Quote,

# termstructure.jl
TermStructure, timeFromReference,
# termstructures/volatility/andreasenhugevolatilityinterpl.jl
CalibrationSet, PreminumFilter,
# termstructures/yieldtermstructure.jl
discount,
# termstructures/yield/discretedividendyieldtermstructure.jl
DiscreteDividendYieldTermStructure,
# termstructures/yield/flatforward.jl
FlatForward,
# termstructure/yield/repoyieldtermstructure.jl
RepoYieldTermStructure,
# termstructure/yield/zerocurve.jl
InterpolatedZeroCurve,
# time
NullCalendar,
# interestrate.jl
InterestRate, Frequency, Continuous, Annual,

# daycounter.jl
yearFraction,

# lazy.jl
LazyMixin,

# observer.jl
ObserverMixin


@enum Frequency NoFrequency Once Annual SemiAnnual Quarterly

include("abstracttypes.jl")
include("exercise.jl")
include("interestrate.jl")
include("observer.jl")
include("lazy.jl")
include("instruments/option.jl")
include("instruments/payoffs.jl")
include("daycounter.jl")
# include("andreasenhugeinterpolation.jl")
include("methods/finitedifferences/layout.jl")
include("methods/finitedifferences/mesher.jl")
include("quotes/quotes.jl")

include("termstructure.jl")
include("termstructures/volatility/andreasenhugevolatilityinterpl.jl")
include("termstructures/yieldtermstructure.jl")
include("termstructures/yield/discretedividendyieldtermstructure.jl")
include("termstructures/yield/flatforward.jl")
include("termstructures/yield/repoyieldtermstructure.jl")
include("termstructures/yield/zerocurve.jl")
include("time/calendar.jl")
include("processes/blackscholesprocess.jl")
end
