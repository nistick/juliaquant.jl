using Test
using Dates

optionType = Put()
underlying = 36.0
strike = 40.0
dividendYield = 0.0
riskFreeRate = 0.06
volatility = 0.2
maturity = Date(1999, 5, 17)
dayCounter = Actual365()
payoff = PlainVanillaPayoff(Call(), strike)
underlyingH = Quote(underlying)
evaluationDate = Date(1998, 5, 15)

sMin = 0.1
sMax = 2.5
Ns = 1001
mesher = Uniform1DMesher(log(sMin), log(sMax), Ns)
days = evaluationDate:Dates.Day(1):maturity |> collect
timegrid = [yearFraction(dayCounter, evaluationDate, x) for x in days]

dtGrid = timegrid[2:end] - timegrid[1:end-1]
