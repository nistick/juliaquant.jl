using Test
using JuliaQuant
import JuliaQuant:Calendar, Actual365, DiscreteDividendYieldTermStructure, NullCalendar
using Dates

today = Date(2017, 2, 27)
cal = NullCalendar();
dc = Actual365();
quotes=Vector{Float64}();
quoteDates= Vector{Date}();

push!(quotes, 0.0015103536741800001)
push!(quotes, 0.0132446399120000010)
push!(quotes, 0.0015059556532900000)
push!(quotes, 0.0132178852850000000)
push!(quotes, 0.0015026571376199999)
push!(quotes, 0.0131914971596000000)
push!(quotes, 0.0014997251236900000)

push!(quoteDates, Date(2017, 6, 28))
push!(quoteDates, Date(2017, 12, 28))
push!(quoteDates, Date(2018, 6, 28))
push!(quoteDates, Date(2018, 12, 28))
push!(quoteDates, Date(2019, 6, 28))
push!(quoteDates, Date(2019, 12, 30))
push!(quoteDates, Date(2020, 6, 29))

divdTS = DiscreteDividendYieldTermStructure(today, 0, dc, cal, quoteDates, quotes);
@test isapprox(discount(divdTS, Date(2017, 3, 29)), 1, atol = 1.e-6)
@test isapprox(discount(divdTS, Date(2017, 5, 29)), 1, atol = 1.0e-6)
@test isapprox(discount(divdTS, Date(2017, 8, 28)), 0.998489646, atol = 1.0e-6)
@test isapprox(discount(divdTS, Date(2017, 11, 27)), 0.998489646, atol = 1.0e-6)
@test isapprox(discount(divdTS, Date(2018, 2, 27)), 0.985265011, atol = 1.0e-6)
@test isapprox(discount(divdTS, Date(2018, 8, 28)), 0.983781245, atol = 1.0e-6)
@test isapprox(discount(divdTS, Date(2019, 2, 27)), 0.970777737, atol = 1.0e-6)
@test isapprox(discount(divdTS, Date(2020, 2, 27)), 0.956532223, atol = 1.0e-6)


quotes = Vector{Float64}()
quoteDates = Vector{Date}()

push!(quotes,-0.0057129572000000012000000000)
push!(quotes,-0.0126516368000000010000000000)
push!(quotes,-0.0039267234999999998000000000)
push!(quotes,-0.0067335936000000006000000000)
push!(quotes,-0.0053375033000000001000000000)
push!(quotes,-0.004087623099999999900000000)
push!(quotes,-0.0036353904999999998000000000)
push!(quotes,-0.0098852589000000008000000000)
push!(quotes,0.0031264582000000004000000000)
push!(quotes,-0.0065982326000000001000000000)
push!(quotes,-0.0044584046000000002000000000)
push!(quotes,-0.004160936199999999700000000)
push!(quotes,-0.0012006958000000002000000000)
push!(quotes,0.0019703042000000000000000000)
push!(quotes,0.0003284631000000000100000000)
push!(quotes,0.0014172335000000001000000000)
push!(quotes,0.0015113497000000000000000000)
push!(quotes,0.002321143200000000000000000)
push!(quotes,0.0000524154000000000000000000)

push!(quoteDates, Date(2017, 3, 9))
push!(quoteDates, Date(2017, 4, 13))
push!(quoteDates, Date(2017, 5, 11))
push!(quoteDates, Date(2017, 6, 8))
push!(quoteDates, Date(2017, 7, 13))
push!(quoteDates, Date(2017, 8, 10))
push!(quoteDates, Date(2017, 9, 14))
push!(quoteDates, Date(2017, 12, 14))
push!(quoteDates, Date(2018, 3, 8))
push!(quoteDates, Date(2018, 6, 14))
push!(quoteDates, Date(2018, 9, 13))
push!(quoteDates, Date(2018, 12, 13))
push!(quoteDates, Date(2019, 12, 12))
push!(quoteDates, Date(2020, 12, 10))
push!(quoteDates, Date(2021, 12, 9))
push!(quoteDates, Date(2022, 12, 8))
push!(quoteDates, Date(2023, 12, 14))
push!(quoteDates, Date(2024, 12, 12))
push!(quoteDates, Date(2025, 12, 11))

repoTS = RepoYieldTermStructure(today, 0, dc, cal, quoteDates, quotes)

@test isapprox(discount(repoTS, Date(2017, 3, 29)), 0.999150601, atol=1.0e-6)
@test isapprox(discount(repoTS, Date(2017, 5, 29)), 0.997999019, atol=1.0e-6)
@test isapprox(discount(repoTS, Date(2017, 8, 28)), 0.996812956, atol=1.0e-6)
@test isapprox(discount(repoTS, Date(2017, 11, 27)), 0.994648781, atol=1.0e-6)
@test isapprox(discount(repoTS, Date(2018, 2, 27)), 0.994829837, atol=1.0e-6)
@test isapprox(discount(repoTS, Date(2018, 8, 28)), 0.992236121, atol=1.0e-6)
@test isapprox(discount(repoTS, Date(2019, 2, 27)), 0.990765895, atol=1.0e-6)
@test isapprox(discount(repoTS, Date(2020, 2, 27)), 0.9902392, atol=1.0e-6)
