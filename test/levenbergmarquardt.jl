using LsqFit
using Logging
using Test
import LsqFit:lmfit
@. model1(x) = 1+ x + x^2
function curvefit(model::Function, p0; kwargs...)
    # construct the cost function
    T = eltype(ydata)
    lmfit(model,p0,T[]; kwargs...)
end

res = curvefit(model1, [0.5])
@test isapprox(res.param[1] ,-0.5, atol = 1.0e-6)
