using Dates
using Distributions
using StatsFuns
import Base:*, + , -
using Logging
using PyCall
@pyimport py_lets_be_rational as lbr


struct BlackScholesProcess
    referenceDate::Date
    spot::Float64
    q::Float64
    r::Float64!
    vol::Float64
end

function forwardPrice(process::BlackScholesProcess, target::Date)
    T = Dates.value(target - process.referenceDate)/365
    return process.spot * exp((process.r-process.q)*T)
end

function totalVariance(process::BlackScholesProcess, target::Date)
    T = Dates.value(target - process.referenceDate)/365
    return process.vol^2 * T
end

abstract type FdmLinearOp end

struct FirstDerivativeOp <: FdmLinearOp
    lower::Vector{Float64}
    diag::Vector{Float64}
    upper::Vector{Float64}
end

function FirstDerivativeOp(dplus::Vector{Float64}, dminus::Vector{Float64})
    lower = -dplus./(dminus.*(dminus+dplus))
    diag = (dplus- dminus)./(dplus.*dminus)
    upper = dminus./(dplus.*(dplus+dminus))
    return FirstDerivativeOp(lower, diag, upper)
end

struct SecondDerivativeOp <: FdmLinearOp
    lower::Vector{Float64}
    diag::Vector{Float64}
    upper::Vector{Float64}
end

function SecondDerivativeOp(dplus::Vector{Float64}, dminus::Vector{Float64})
    zetam1 = dminus.*(dminus+dplus)
    zeta0 = dminus.*dplus
    zetap1 = dplus.*(dminus+dplus)
    lower = 2.0./zetam1
    diag = -2.0./zeta0
    upper = 2.0./zetap1
    return SecondDerivativeOp(lower, diag, upper)
end

function *(op::T, r::Float64) where T <: FdmLinearOp
    lower = op.lower .* r
    diag = op.diag .* r
    upper = op.upper .* r
    return T(lower, diag, upper)
end

function *(r::Float64, op::T) where T <: FdmLinearOp
    lower = op.lower .* r
    diag = op.diag .* r
    upper = op.upper .* r
    return T(lower, diag, upper)
end

function +(op::T, r::Float64) where T <: FdmLinearOp
    lower = op.lower .+ r
    diag = op.diag .+ r
    upper = op.upper .+ r
    return T(lower, diag, upper)
end
function -(op::T, r::Float64) where T <: FdmLinearOp
    lower = op.lower - r
    diag = op.diag - r
    upper = op.upper - r
    return T(lower, diag, upper)
end

function +(op1::T1, op2::T2) where {T1, T2 <: FdmLinearOp}
    lower = op1.lower + op2.lower
    diag = op1.diag + op2.diag
    upper = op1.upper + op2.upper
    return TripleBandOp(lower, diag, upper)
end



today = Date(2018, 2, 18)
spot = 100
strike = 100
qTS = 0.06
rTS = 0.1
volTS = 0.35
maturity = today + Dates.Month(6)
Nt = 15
Ns = 100
T = Dates.value(maturity - today)/365
dt = T/Nt
eps_ = 0.0001
scalarFactor = 1.5

mutable struct TripleBandOp <: FdmLinearOp
    lower::Vector{Float64}
    diag::Vector{Float64}
    upper::Vector{Float64}
end

function TripleBandOp(size::Int)
    lower = zeros(size)
    diag = zeros(size)
    upper = zeros(size)
    return TripleBandOp(lower, diag, upper)
end

struct BlackScholesOp
    Ns::Int
    process::BlackScholesProcess
    dxMap::FirstDerivativeOp
    dxxMap::SecondDerivativeOp
    Map_::TripleBandOp
end

function axpyb(op::BlackScholesOp, a::Float64, x::T1, y::T2, b::Vector{Float64}) where {T1, T2 <: FdmLinearOp}
    ax = a * x
    op.Map_.diag = ax.diag .+ y.diag .+ b
    op.Map_.lower = ax.lower .+ y.lower
    op.Map_.upper = ax.upper .+ y.upper
end

function setTime(op::BlackScholesOp, t1::Float64, t2::Float64)
    r = op.process.r
    q = op.process.q
    v = op.process.vol^2
    b = fill(-r, op.Ns)
    axpyb(op, (r-q-v/2), op.dxMap, op.dxxMap*(0.5*v), b)
end


function solve_split(op::BlackScholesOp, r::Vector{Float64}, dt::Float64, b::Float64)
    # @info "solve_split values" dt b
    # @info "r" r[1:5]
    lower = op.Map_.lower .* dt
    diag = op.Map_.diag .* dt .+ b
    upper = op.Map_.upper .* dt
    lower[1] = 0.0
    diag[1] = 1.0
    upper[1] = 0.0
    lower[end] = 0.0
    diag[end] = 1.0
    upper[end] = 0.0
    β = 1/diag[1]
    retVal = zeros(length(lower))
    tmp = zeros(length(lower))
    retVal[1] = r[1]*β
    tmp[1] = upper[1]*β
    for i in range(2, stop=length(lower))
        β = 1/(diag[i]-tmp[i-1]*lower[i])
        tmp[i] = upper[i]*β
        retVal[i] = (r[i]-retVal[i-1]*lower[i])*β
    end
    # @info tmp[1]
    for i in range(length(lower)-1, stop=2, step=-1)
        retVal[i] = retVal[i] - tmp[i]*retVal[i+1]
    end

    # @info "retVal" retVal[1:5]
    return retVal
end

function step_(op::BlackScholesOp, a::Vector{Float64}, t::Float64)
    # @info t t-dt
    setTime(op, max(0.0, t -dt), t)
    a = solve_split(op, a, -dt, 1.0)
    # @info a[1:5]
    return a
end

function evolve(op::BlackScholesOp, a::Vector{Float64}, from::Float64, to::Float64, steps::Int)
    dt = (from - to ) /steps
    t = from
    rhs = a
    for i in range(1, stop=steps)
        # if i > 2
        #     break
        # end
        # @info i
        # @info rhs[1:5]
        rhs = step_(op, rhs, t)
        t -= dt
    end
    return rhs
end

process = BlackScholesProcess(today, spot, qTS, rTS, volTS)
fwd = forwardPrice(process, maturity)
normInvEps = quantile(Normal(), 1-eps_)
# @info normInvEps
normInvEps = 3.7190164821251308
sqrtSigma = sqrt(totalVariance(process, maturity))

xMin = log(process.spot) - sqrtSigma*normInvEps*scalarFactor
xMax = log(fwd) + sqrtSigma*normInvEps*scalarFactor
cPoint = log(process.spot)
density = 0.1 * (xMax-xMin)
dx = 1/(Ns-1)
c1 = asinh((xMin - cPoint)/density)
c2 = asinh((xMax - cPoint)/density)
x = range(0.0, stop=1.0, length=Ns) |> collect
locations = cPoint.+density*sinh.(c1*(1.0.-x) + c2*x)
locations[1] = xMin
locations[end] = xMax
# locations = range(log(0.1*spot), length=Ns, stop=log(2.5*spot)) |> collect
dplus = locations[2:end]-locations[1:end-1]
push!(dplus, -1)
dminus = locations[2:end] - locations[1:end-1]
pushfirst!(dminus, -1)

payoff(x) = max(strike-x, 0.0)
calculator(x) = begin
    ex = exp(x)
    payoff(ex)
end

rhs = calculator.(locations)

lower = zeros(length(rhs))
diag = zeros(length(rhs))
upper = zeros(length(rhs))
T = Dates.value(maturity-today)/365
dt = T/Nt
dxMap = FirstDerivativeOp(dplus, dminus)
dxxMap = SecondDerivativeOp(dplus, dminus)
dMap = BlackScholesOp(Ns, process, dxMap, dxxMap, TripleBandOp(Ns))
# @info rhs[1:5]
y = evolve(dMap, rhs, T, 0.0, Nt)

using Dierckx

spl = Spline1D(locations, y, k = 3)


@info evaluate(spl, log(100))


df = exp(-rTS*T)
fwd = spot * exp((rTS-qTS)*T)
jaeckel = lbr.black(fwd, strike, volTS, T, -1.0)*df
@info jaeckel - evaluate(spl, log(100))

t = 0.25
spot = 60
strike = 65
r = 0.08
vol = 0.3
fwd = spot * exp(r*t)
df = exp(-r*t)
@info lbr.black(fwd, strike, vol, t, 1)*df
@info lbr.black(fwd, strike, vol, t, 1)
