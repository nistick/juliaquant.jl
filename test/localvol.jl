using Test
using JuliaQuant
using Logging
using Dates

today = Date(2018, 1, 4)
dc = Actual365()
rTS = FlatForward(today, Quote(0.01), dc)
qTS = FlatForward(today, Quote(0.03), dc)
maturity = today + Dates.Year(1)

exercise = EuropeanExercise(maturity)
payoff = PlainVanillaPayoff(Call(), 120)
