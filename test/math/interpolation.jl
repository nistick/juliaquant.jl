using Dierckx

x = range(0.0, stop=2π, length=8)
y = sin.(x)
spl = Spline1D(x, y, k=1)
