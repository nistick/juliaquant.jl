push!(LOAD_PATH, "e:/dev/finance/julia")
using JuliaQuant


tests = [
        # "payoff",
         # "fdmeuropeanoption",
         # "yieldtermstructure",
         # "forwardprice",
         "andreasenhugevolinterpl",
         # "localvol"
         # "levenbergmarquardt"
         ]

for t in tests
    tfile = string(t, ".jl")
    println(" * $(tfile) ...")
    include(tfile)
end
