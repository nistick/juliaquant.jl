using Test
using JuliaQuant

optionType = JuliaQuant.Call()
strike = 100.0
price = 110.0

payoff = PlainVanillaPayoff(optionType, strike)

println(payoff(price))
