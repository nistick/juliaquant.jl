using Test
using Dates
using JuliaQuant

today = Date(2018, 10, 15)
dc = Actual365()
cal = NullCalendar()
divd = Vector{Float64}()
divdDates = Vector{Date}()
push!(divd, 0.016397229337277698);push!(divdDates, Date(2018,12,27))
push!(divd, 0.002362902702117680);push!(divdDates, Date(2019,3,28))
push!(divd, 0.003151621631371980);push!(divdDates, Date(2019,6,27))
push!(divd, 0.002362902702117680);push!(divdDates, Date(2019,9,27))
push!(divd, 0.017781608283127099);push!(divdDates, Date(2019,12,27))
push!(divd, 0.000077686785237563);push!(divdDates, Date(2020,3,28))
push!(divd, 0.002288585446805440);push!(divdDates, Date(2020,3,30))
push!(divd, 0.000080363649482305);push!(divdDates, Date(2020,6,27))
push!(divd, 0.003114131101410580);push!(divdDates, Date(2020,6,29))
push!(divd, 0.000077686785237563);push!(divdDates, Date(2020,9,27))
push!(divd, 0.002288585446805440);push!(divdDates, Date(2020,9,29))
push!(divd, 0.000660059886720300);push!(divdDates, Date(2020,12,27))
push!(divd, 0.018507644575922699);push!(divdDates, Date(2020,12,29))
push!(divd, 0.000077686785237563);push!(divdDates, Date(2021,3,28))
push!(divd, 0.002288585446805440);push!(divdDates, Date(2021,3,30))
push!(divd, 0.000080363649482305);push!(divdDates, Date(2021,6,27))
push!(divd, 0.003114131101410580);push!(divdDates, Date(2021,6,29))
push!(divd, 0.000077686785237563);push!(divdDates, Date(2021,9,27))
push!(divd, 0.002288585446805440);push!(divdDates, Date(2021,9,29))
push!(divd, 0.000660059886720300);push!(divdDates, Date(2021,12,27))
push!(divd, 0.018507644575922699);push!(divdDates, Date(2021,12,29))
push!(divd, 0.000077686785237563);push!(divdDates, Date(2022,3,28))
push!(divd, 0.002288585446805440);push!(divdDates, Date(2022,3,30))
push!(divd, 0.000080363649482305);push!(divdDates, Date(2022,6,27))
push!(divd, 0.003114131101410580);push!(divdDates, Date(2022,6,29))
qTS = DiscreteDividendYieldTermStructure(today, 0, dc, cal, divdDates, divd)

repos = Vector{Float64}()
repoDates = Vector{Date}()
push!(repos, -0.0017009459000000002378699682);push!(repoDates, Date(2018,11,8))
push!(repos, 0.0000437968000000000005304118);push!(repoDates, Date(2018,12,13))
push!(repos, 0.0228729605000000009440430659);push!(repoDates, Date(2019,1,10))
push!(repos, -0.0017802110000000000763348273);push!(repoDates, Date(2019,3,14))
push!(repos, -0.0023400124999999999918232074);push!(repoDates, Date(2019,6,13))
push!(repos, -0.0027222753999999998314462957);push!(repoDates, Date(2019,9,11))
push!(repos, -0.0036994360000000003907416612);push!(repoDates, Date(2019,12,12))
push!(repos, 0.0014158854000000001098491520);push!(repoDates, Date(2020,6,11))
push!(repos, -0.0039773900999999995189981838);push!(repoDates, Date(2020,12,10))
push!(repos, 0.0036964373999999996026066817);push!(repoDates, Date(2021,6,10))
push!(repos, -0.0028223283999999999524521677);push!(repoDates, Date(2021,12,9))
pTS = RepoYieldTermStructure(today, 0, dc, cal, repoDates, repos)


rates = Vector{Float64}()
curveDates = Vector{Date}()
push!(curveDates, Date(2018, 10, 15))
push!(curveDates, Date(2018, 10, 16))
push!(curveDates, Date(2018, 10, 17))
push!(curveDates, Date(2019, 1, 16))
push!(curveDates, Date(2019, 4, 16))
push!(curveDates, Date(2019, 7, 16))
push!(curveDates, Date(2019, 10, 16))
push!(curveDates, Date(2020, 4, 16))
push!(curveDates, Date(2020, 10, 16))
push!(curveDates, Date(2021, 10, 18))
push!(curveDates, Date(2022, 10, 17))
push!(curveDates, Date(2023, 10, 16))
push!(curveDates, Date(2024, 10, 16))
push!(curveDates, Date(2025, 10, 16))
push!(curveDates, Date(2026, 10, 16))
push!(curveDates, Date(2027, 10, 18))
push!(curveDates, Date(2028, 10, 16))
push!(curveDates, Date(2030, 10, 16))
push!(curveDates, Date(2033, 10, 17))
push!(curveDates, Date(2038, 10, 18))
push!(rates, 0.014999692)
push!(rates, 0.014999692)
push!(rates, 0.014999692)
push!(rates, 0.016647045)
push!(rates, 0.017872065)
push!(rates, 0.018424474)
push!(rates, 0.018851324)
push!(rates, 0.019429842)
push!(rates, 0.019807811)
push!(rates, 0.02028808)
push!(rates, 0.020695494)
push!(rates, 0.021028512)
push!(rates, 0.021206831)
push!(rates, 0.021360927)
push!(rates, 0.021543881)
push!(rates, 0.021783194)
push!(rates, 0.022024855)
push!(rates, 0.022374785)
push!(rates, 0.022409822)
push!(rates, 0.022385398)
rTS = InterpolatedZeroCurve(0, today, dc, cal, JuliaQuant.Math.LinearInterpolation(), curveDates, rates)


"""
 KOSPI 200 Index Forward Price in 2018-10-15
2018-11-08	277.4397567275
2018-12-13	277.8793285656
2019-01-10	274.1729388056
2019-03-14	274.9721603168
2019-06-13	275.5016445524
2019-09-11	275.8013526364
2019-12-12	276.3008660636
2020-06-11	273.7533475156
2020-12-10	274.5525690269
2021-06-10	272.0050504512
2021-12-09	273.0340481417
"""

function forwardPrice(spot, r, q, p, d)
  df = discount(r, d)
  div = discount(q, d)
  repo = discount(p, d)
  return spot/df*repo*div
end
spot =277.19
d1 = Date(2018, 11, 8)
@test isapprox(forwardPrice(spot, rTS, qTS, pTS, d1), 277.4397567275, atol=1.0e-6)
d2 = Date(2019, 9, 11)
@test isapprox(275.8013526364, forwardPrice(spot, rTS, qTS, pTS, d2), atol=1.0e-6)
d3 = Date(2021, 12, 9)
@test isapprox(273.0340481417, forwardPrice(spot, rTS, qTS, pTS, d3), atol=1.0e-6)
d4 = Date(2021, 6, 10)
@test isapprox(272.0050504512, forwardPrice(spot, rTS, qTS, pTS, d4), atol=1.0e-6)
